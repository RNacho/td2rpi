#include <wiringPiI2C.h> // -lwiringPi
#include <wiringPi.h> // -lwiringPi
#include <ncurses.h> // -lncurses
#include <stdlib.h>
#include <stdio.h>

const char ADC = 0x48; // direccion base

int fd, valor;	//declaracion de variables para acceder al adc

int main(void){
	if (wiringPiSetup() == -1) exit (1);	//
	fd = wiringPiI2CSetup(ADC);		// configuracion del bus I2C, inicializa el sistema I2C con el ID del dispositivo.
	while(1){
		wiringPiI2CReadReg8(fd, ADC + 0) ;
		valor = wiringPiI2CReadReg8(fd, ADC + 0);
		printf("Potenciometro = %d \n", valor);
		
		wiringPiI2CReadReg8(fd, ADC + 1) ; 
		valor = wiringPiI2CReadReg8(fd, ADC + 1);
		printf("Fotocelula = %d \n", valor);

		wiringPiI2CReadReg8(fd, ADC + 2) ; 
		valor = wiringPiI2CReadReg8(fd, ADC + 2);
		printf("Thermistor = %d \n\n", valor);

		delay(500);
	}
	return 0;
}

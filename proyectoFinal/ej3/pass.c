#include <ncurses.h> // -lncurses

int main(void){
	initscr();		// Inicializo la pantalla de ncurses
	noecho(); 		// Deshabilito el echo automatico de los caracteres tipeados
  	keypad(stdscr, TRUE); 	// Habilito que se pueda usar el keypad del teclado en la pantalla de ncurses

	int caracter;		// Variable que guarda el caracter
	char cadena[5] = {'0','0','0','0','0'};		// Cadena del pass ingresado
	char password[5] = {'s','e','n','i','a'};	// Cadena del pass correcto

	int x=24;		// Contador de posicion horizontal en pantalla

	mvaddstr(0,0,"Ingrese la contraseña: ");

	// Comienza recepcion de datos
	while ( caracter != '\n'){	// Siempre y cuando no me llegue un enter (KEY_ENTER de ncurses no funca)
		caracter = getch();		// Guardo el caracter tipeado
		switch (caracter){		// Depende del caracter realizo una accion
			case KEY_BACKSPACE:		// Si me llego un espacio
				if(x-24>0){			// Y no estoy ya al borde de la pantalla
					x--;			// Me muevo para la izquierda en la pantalla, y en el arreglo de cadena
					mvaddch(0,x,' ');	// Y borro lo que habia escrito ahi
				}
				break;
			case KEY_UP:			// Si me llega una flecha no hago nada, por desicion de diseño
				break;
			case KEY_DOWN:			// Si me llega una flecha no hago nada, por desicion de diseño
				break;
			case KEY_LEFT:			// Si me llega una flecha no hago nada, por desicion de diseño
				break;
			case KEY_RIGHT:			// Si me llega una flecha no hago nada, por desicion de diseño
				break;
			case '\n':			// Si me llega un enter no hago nada, en el prox ciclo se saldra del switch con esto
				break;
			default:			// Con cualquier otro caracter que me llegue
				mvaddch(0,x,'*');		// Agrego un asterisco en pantalla
				if(x-24<5){
					cadena[x-24] = caracter;		// Agrego el caracter que me llego a la cadena de pass ingresados
				}
				x++;				// Muevo mi cursor a la derecha en la pantalla , y en el arreglo de cadena
				break;
			}
		wrefresh(stdscr); 		//Refresco pantalla, necesario segun documentacion de ncurses
	}
	// Termina recepcion de datos
	
	bool igualdad = 1;	// Booleano que me sirve para comparar las dos cadenas

	mvaddstr(2,0,"[DEBUG] La contraseña era: ");	// En la segunda linea imprimo cadena de debug con el pass correcto
	mvaddstr(3,0,"[DEBUG] Usted ingresó: ");	// En la tercera linea imprimo cadena de debug con el pass ingresado
	
	for(int i=0; i<5; i++){		// Recorro la cadena de passwords, que por diseño es de 5 caracteres
		mvaddch(2,i+28,password[i]);			// Agrego en pantalla, segunda linea de debug, pass correcta
		mvaddch(3,i+24,cadena[i]);			// Agrego en pantalla, tercera linea de debug, pass ingresada
		igualdad = igualdad & (cadena[i]==password[i]); // Operacion AND de booleano igualdad, e igualdad entre caracteres,
								//   cuando encuentre una desigualdad entre cadena[i] y password[i]
								//   de ahi en adelante siempre se quedara igualdad en 0
	}

	if (igualdad){	// Si se comprobo que todos los caracteres de las cadenas son iguales
		mvaddstr(5,0,"Bienvenido al Sistema");
	}else{		// Si hubo desigualdad en al menos un caracter de las cadenas
		mvaddstr(5,0,"Password no válida");
	}
	
	wrefresh(stdscr); //Refresco pantalla, necesario segun documentacion de ncurses
	getch();	  // Espero que el usuario presione una tecla para finalizar

	endwin();	  // Cierro pantalla de ncurses, necesario segun documentacion
	return 0;	  // Retorno 0 con main
}

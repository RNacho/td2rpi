#include <ncurses.h> // -lncurses
#include "../headers/deteccionTecla.h"

bool logueo(void){

	int caracter;		// Variable que guarda el caracter
	char cadena[5] = {'0','0','0','0','0'};		// Cadena del pass ingresado
	char password[5] = {'s','e','n','i','a'};	// Cadena del pass correcto

	int x=0;		// Contador de posicion horizontal en pantalla
	int posC;		// Variable intermedia para verificar la posicion de columnas

	int filas, columnas;

	// Comienza recepcion de datos
	while ( caracter != '\n'){	// Siempre y cuando no me llegue un enter (KEY_ENTER de ncurses no funca)
		wclear(stdscr);	// Limpio la pantalla al comienzo de cada ciclo
 		getmaxyx(stdscr, filas, columnas); // Guardo en mis variables las filas 

		(columnas/2-22)<0 ? (posC=0) : (posC = columnas/2-22);
 		mvprintw(0	, 0	, "Presione la tecla \"Escape\" para finalizar", filas, columnas);
 		mvprintw(filas/2, posC	, "Ingrese la contraseña y luego la tecla Enter:");
 		mvprintw(filas-1, 0	, "[DEBUG] La terminal tiene %d filas y %d columnas", filas, columnas);

		for(int y=0; y<x; y++){
			mvaddch(filas/2+1,columnas/2-3+y,'*');		// Agrego un asterisco en pantalla
		}

		wrefresh(stdscr); 		//Refresco pantalla, necesario segun documentacion de ncurses

		caracter = getch();		// Guardo el caracter tipeado
		switch (caracter){		// Depende del caracter realizo una accion
			case KEY_BACKSPACE:		// Si me llego un espacio
				if(x>0){			// Y no estoy ya al borde de la pantalla
					x--;			// Me muevo para la izquierda en la pantalla, y en el arreglo de cadena
				}
				break;
			case KEY_RESIZE:		// Me llega este codigo cuando cambia de tamaño la ventana
				break;
			case KEY_UP:			// Si me llega una flecha no hago nada, por desicion de diseño
				break;
			case KEY_DOWN:			// Si me llega una flecha no hago nada, por desicion de diseño
				break;
			case KEY_LEFT:			// Si me llega una flecha no hago nada, por desicion de diseño
				break;
			case KEY_RIGHT:			// Si me llega una flecha no hago nada, por desicion de diseño
				break;
			case 27:
				ungetch(caracter);
				return 0;
			case '\n':			// Si me llega un enter no hago nada, en el prox ciclo se saldra del switch con esto
				break;
			default:			// Con cualquier otro caracter que me llegue
				if(x<5){
					cadena[x] = caracter;		// Agrego el caracter que me llego a la cadena de pass ingresados
				}
				x++;				// Muevo mi cursor a la derecha en la pantalla , y en el arreglo de cadena
				break;
			}
	}
	// Termina recepcion de datos
	
	bool igualdad = 1;	// Booleano que me sirve para comparar las dos cadenas
	
	for(int i=0; i<5; i++){		// Recorro la cadena de passwords, que por diseño es de 5 caracteres
		igualdad = igualdad & (cadena[i]==password[i]); // Operacion AND de booleano igualdad, e igualdad entre caracteres,
								//   cuando encuentre una desigualdad entre cadena[i] y password[i]
								//   de ahi en adelante siempre se quedara igualdad en 0
	}

	if (igualdad){	// Si se comprobo que todos los caracteres de las cadenas son iguales
		while(!deteccionTecla('\n', 0)){
			wclear(stdscr);	// Limpio la pantalla al comienzo de cada ciclo
 			getmaxyx(stdscr, filas, columnas); // Guardo en mis variables las filas 

			(columnas/2-10)<0 ? (posC=0) : (posC = columnas/2-10);
 			mvprintw(0	, 0	, "Presione la tecla \"Enter\" para finalizar");
 			mvprintw(filas/2, posC	, "Bienvenido al sistema");
 			mvprintw(filas-1, 0	, "[DEBUG] La terminal tiene %d filas y %d columnas", filas, columnas);
			wrefresh(stdscr); //Refresco pantalla, necesario segun documentacion de ncurses
		}
		return 1;
	}else{		// Si hubo desigualdad en al menos un caracter de las cadenas
		while(!deteccionTecla('\n', 0)){
			wclear(stdscr);	// Limpio la pantalla al comienzo de cada ciclo
 			getmaxyx(stdscr, filas, columnas); // Guardo en mis variables las filas 

			(columnas/2-8)<0 ? (posC=0) : (posC = columnas/2-8);
 			mvprintw(0	, 0	, "Presione la tecla \"Enter\" para continuar");
 			mvprintw(filas/2, posC	, "Acceso denegado");
 			mvprintw(filas-1, 0	, "[DEBUG] La terminal tiene %d filas y %d columnas", filas, columnas);
			wrefresh(stdscr); //Refresco pantalla, necesario segun documentacion de ncurses
		}
		return 0;
	}
}

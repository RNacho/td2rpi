#include <ncurses.h>
#include <wiringPi.h>
#include "../headers/deteccionTecla.h"
#include "../headers/rs232.h"

int controlVelocidad(int periodo, int puertochar, int pulsadores[2]){
	if((deteccionTecla(KEY_UP, 1) || puertochar=='s' || digitalRead(pulsadores[0])) && periodo>5){
		periodo = periodo - 5;
		//mvprintw(2,0,"Periodo: %4d ms", periodo);
 		//wrefresh(stdscr); // Actualizo la pantalla en cada ciclo
	}
	if((deteccionTecla(KEY_DOWN, 1) || puertochar=='b' || digitalRead(pulsadores[1])) && periodo<9995){
		periodo = periodo + 5;
		//mvprintw(2,0,"Periodo: %4d ms", periodo);
 		//wrefresh(stdscr); // Actualizo la pantalla en cada ciclo
	}
	return periodo;
}

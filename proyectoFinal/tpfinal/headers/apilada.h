#ifndef APILADA_H
#define APILADA_H

int apilada(int leds[8], int llaves[4], int pulsadores[2], int periodo, int fdpuerto, bool remoto, bool standalone);

#endif // APILADA_H

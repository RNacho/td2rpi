#ifndef EXPLOSION_H
#define EXPLOSION_H

int explosion(int pines[8], int llaves[4], int pulsadores[2], int periodo, int fdpuerto, bool remoto, bool standalone);

#endif // EXPLOSION_H

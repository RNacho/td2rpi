#ifndef CONTADOR_H
#define CONTADOR_H

int contador(int leds[8], int llaves[4], int pulsadores[2], int periodo, int fdpuerto, bool remoto, bool standalone);

#endif // CONTADOR_H

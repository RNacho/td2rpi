#ifndef CARRERA_H
#define CARRERA_H

int carrera(int pines[8], int llaves[4], int pulsadores[2], int periodo, int fdpuerto, bool remoto, bool standalone);

#endif // CARRERA_H

#ifndef LOTERIA_H
#define LOTERIA_H

int loteria(int leds[8], int llaves[4], int pulsadores[2], int periodo, int fdpuerto, bool remoto, bool standalone);

#endif // LOTERIA_H

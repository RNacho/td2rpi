#ifndef POLICIA_H
#define POLICIA_H

int policia(int pines[8], int llaves[4], int pulsadores[2], int periodo, int fdpuerto, bool remoto, bool standalone);

#endif // POLICIA_H
